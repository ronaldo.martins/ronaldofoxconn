﻿using RonaldoFoConnWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RonaldoFoConnWeb.Interface
{
    interface IProductRepository
    {
        IEnumerable<TblProduct> GetAll();
        TblProduct Get(int id);
        TblProduct Add(TblProduct item);
        bool Update(TblProduct item);
        bool Delete(Guid id);
        bool Delete(int id);
    }
}
