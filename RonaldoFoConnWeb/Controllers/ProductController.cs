﻿using RonaldoFoConnWeb.Interface;
using RonaldoFoConnWeb.Models;
using RonaldoFoConnWeb.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RonaldoFoConnWeb.Controllers
{
    public class ProductController : ApiController
    {
        static readonly IProductRepository repository = new ProductRepository();

        public IEnumerable GetAllProducts()
        {
            return repository.GetAll();
        }

        public IEnumerable Graph()
        {
            return repository.GetAll();
        }

        


        public TblProduct PostProduct(TblProduct item)
        {
            return repository.Add(item);
        }

        public IEnumerable PutProductGuid(Guid id, TblProduct product)
        {
            product.Id = id;

            if (repository.Update(product))
            {
                return repository.GetAll();
            }
            else
            {
                return null;
            }
        }

        public IEnumerable PutProduct(int id, TblProduct product)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(id).CopyTo(bytes, 0);

            product.Id = new Guid(bytes);

            if (repository.Update(product))
            {
                return repository.GetAll();
            }
            else
            {
                return null;
            }
        }

        public bool DeleteProductGuid(Guid id)
        {
            if (repository.Delete(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteProduct(int id)
        {
            if (repository.Delete(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}