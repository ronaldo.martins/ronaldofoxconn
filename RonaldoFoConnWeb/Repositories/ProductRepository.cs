﻿using RonaldoFoConnWeb.Interface;
using RonaldoFoConnWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RonaldoFoConnWeb.Repositories
{
    public class ProductRepository : IProductRepository
    {
        FoxConnDatabaseEntities ProductDB = new FoxConnDatabaseEntities();

        public IEnumerable<TblProduct> GetAll()
        {
            // Get the list of all the records in database
            return ProductDB.TblProduct;
        }

        public TblProduct Get(int id)
        {
            // Code to find a record in database
            return ProductDB.TblProduct.Find(id);
        }

        public TblProduct Add(TblProduct item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            item.Id = Guid.NewGuid();

            // Save record into database
            ProductDB.TblProduct.Add(item);
            ProductDB.SaveChanges();
            return item;
        }

        public bool Update(TblProduct item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            // Code to update record into database
            var products = ProductDB.TblProduct.Single(a => a.Id == item.Id);
            products.Name = item.Name;
            products.Category = item.Category;
            products.Price = item.Price;
            ProductDB.SaveChanges();

            return true;
        }

        public bool Delete(Guid id)
        {
            // Code to remove the records from database
            TblProduct products = ProductDB.TblProduct.Find(id);
            ProductDB.TblProduct.Remove(products);
            ProductDB.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            // Code to remove the records from database
            TblProduct products = ProductDB.TblProduct.Find(id);
            ProductDB.TblProduct.Remove(products);
            ProductDB.SaveChanges();
            return true;
        }
    }
}